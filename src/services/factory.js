export function nodeByType(type) {
    const data = typeNodes[type] ?? {};

    return {
        id: Math.random().toString(),
        position: {
            x: Math.random().toString() * 500,
            y: Math.random().toString() * 500,
        },
        data,
        type,
        label: Math.random().toString(),
        selected: false,
        connectable: false,
        dimensions: {
            height: 0,
            width: 0,
        },
        dragging: true,
        resizing: false,
        'zIndex': 1,
        events: {

        }
    }
}

export function isTypeExist(type) {
    return Object.keys(typeNodes).includes(type);
}

const typeNodes = {
    conveyor: {
        isOn: true,
    },
    conveyor_sensor: {
        isOn: true,
        sensor: true,
    },
    boiler: {

    },
    control_button: {

    },
}