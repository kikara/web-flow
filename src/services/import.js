import {nodeByType, isTypeExist} from "@/services/factory.js";

export function importFile(callback = () => {}) {
    const inputElement = document.createElement('input');
    inputElement.type = 'file';
    inputElement.accept = '.json';
    inputElement.onchange = () => {
        const file = inputElement.files[0];

        readJsonFile(file)
            .then(object => {

                const nodes = [];
                for (const [key, value] of Object.entries(object)) {

                    const isExist = isTypeExist(key);

                    if (!isExist) {
                        console.warn('type is not recognized: ' + key);
                        continue;
                    }

                    const defaultNode = nodeByType(key);
                    console.log(value);

                    defaultNode.data = {...defaultNode.data, ...value};

                    console.log(defaultNode.data);
                    nodes.push(defaultNode);
                }

                callback(nodes);
            })
            .catch(error => {
                console.error(error);
            });

    };
    inputElement.click();
}

async function readJsonFile(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => {
            try {
                const object = JSON.parse(reader.result);
                resolve(object);
            } catch (error) {
                reject(error);
            }
        };
        reader.onerror = reject;
        reader.readAsText(file);
    });
}