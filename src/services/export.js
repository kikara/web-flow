export function exportData(nodes) {
    const out = {};
    for (const node of nodes) {
        out[node.type] = node.data;
    }

    downloadJson(out);
}

function downloadJson(objectToDownload) {
    const jsonString = JSON.stringify(objectToDownload, null, 2);
    const blob = new Blob([jsonString], { type: 'application/json' });
    const link = document.createElement('a');
    link.href = URL.createObjectURL(blob);
    link.download = 'download.json';
    link.click();
    URL.revokeObjectURL(link.href);
}