export function parameterByType(type) {
    return {...types[type]};
}

const types = {
    personal_shape: {
        name: {
            type: 'input'
        },
        color: {
            type: 'input'
        },
        shape_type: {
            type: 'select',
            variants: [
                'square',
                'ellipse'
            ]
        },
        width: {
            type: 'input'
        },
        height: {
            type: 'input'
        },
    },
    bool: {
        name: {type: 'input'},
        on_color: {type: 'input'},
        off_color: {type: 'input'},
        width: {type: 'input'},
        height: {type: 'input'},
        interval: {type: 'input'},
    },
    text_button: {
        name: {type: 'input'},
    }
}