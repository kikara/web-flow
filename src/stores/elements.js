import {defineStore} from "pinia";
import {ref} from "vue";

export const useElementsStore = defineStore('elements', () => {

    const elements = ref([
        {
            id: 1,
            label: 'Boiler',
            type: 'boiler',
            position: {
                x: Math.random() * 500,
                y: Math.random() * 500,
            }
        },
        {
            id: 2,
            label: 'conveyor',
            type: 'conveyor',
            position: {
                x: 400,
                y: 200,
            }
        },
        {
            id: 3,
            label: 'conveyor_sensor',
            type: 'conveyor_sensor',
            position: {
                x: 400,
                y: 200,
            }
        },
        {
            id: 4,
            label: 'control_button',
            type: 'control_button',
            position: {
                x: 400,
                y: 200,
            }
        },
    ]);


    return {elements};
});